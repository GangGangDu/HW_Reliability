__author__ = 'maying03'
import threading
import time
from run_test.log import Logger
from run_test import winTools_operate, builder, printer_operate, Configure

class coredumpMonitor(threading.Thread): #The timer class is derived from the class threading.Thread
    def __init__(self,interval):
        threading.Thread.__init__(self)
        self.logger = Logger.getLogger()
        self.interval = interval
        self.thread_stop = False

    def run(self): #Overwrite run() method, put what you want the thread do here
        while not self.thread_stop:
            self.logger.debug('Check whether coredump happened, Time:%s\n' %(time.ctime()))
            builder1 = builder.TestBuilder.getBuilder()
            printer_ip = builder1.getConfig(property = 'printer_ip')
            self.logger.debug('print ip is %s'%printer_ip)
            flag = printer_operate.isCoreDump(printer_ip)
            if flag == True:
                self.logger.debug('Assert happened, catch log...')
                test_result_folder = '%s%s%s'%(Configure.REPORT_DIR,'/', builder1.getRunningTestName())
                winTools_operate.captureCoredump(test_result_folder)
            time.sleep(self.interval)

    def stop(self):
        self.thread_stop = True
