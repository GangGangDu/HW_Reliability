'''General Automation Test Framework by Beyondsoft TSCT
Usage: Executor_Windows_Python.run()
Note: check version of the file by printing file.ver'''
ver = '0.9.0'
from Tkinter import *
import ttk
import tkFont

roottk = None
text = None
result = False
comment = ""

def get_screen_size(window):
	return window.winfo_screenwidth(), window.winfo_screenheight()

def get_window_size(window):
	return window.winfo_reqwidth(), window.winfo_reqheight()
	
def center_window(root, width, height):
	screenwidth=root.winfo_screenwidth()
	screenheight = root.winfo_screenheight()
	size = '%dx%d+%d+%d' % (width, height,  (screenwidth -width)/2, (screenheight - height)/2)
	root.geometry(size)
	
def on_clickyes():
  global roottk
  global result
  result = True
  roottk.destroy()

def on_clickno():
  global roottk
  global result
  result = False
  global comment
  global text
  comment = text.get()
  roottk.destroy()

def showinfo(info):
  global roottk
  roottk = Tk()
  roottk.title("Semi-Auto")
  center_window(roottk, 600, 480)
  roottk.maxsize(600, 800)
  roottk.minsize(300, 240)

  label1 = Label(roottk)
  label1["text"]=info
  label1["fg"]='red'
  label1["font"]= tkFont.Font(family='Fixdsys',size = 10,weight = tkFont.BOLD)
  label1.pack(expand="yes")
  
  button1 = Button(roottk, width = 20)
  button1["text"]="Continue"
  button1["command"]=on_clickyes
  button1.pack(expand="yes")
  
  #label = Label(roottk)
  #label['text'] = 'Fail Comment'
  #label.pack()
  
  #global text
  #textcontent = StringVar()
  #text = Entry(roottk, textvariable = textcontent)
  #text.pack()
  
  #button2 = Button(roottk, width = 20)
  #button2["text"]="Fail"
  #button2["command"]=on_clickno
  #button2.pack(expand="yes")

  roottk.mainloop()
  #global result
  #global comment
  #if result:
  #  return info
  #else:
  #  return comment
