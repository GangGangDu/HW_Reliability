__author__ = 'maying03'
import  unittest
from run_test.log import Logger
from run_test import winGuiAuto,KeyBoardAction, builder,printer_operate, winTools_operate
from run_test import semiauto
import time, random

class allmediasizes_general_test(unittest.TestCase):

    def setUp(self):
        self.logger = Logger.getLogger()
        self.builder = builder.TestBuilder.getBuilder()
        self.basPath = self.builder.getScript(case_name = 'allmediasizes_general_test')
        winTools_operate.startFlexScript()
    def tearDown(self):
        pass

    def testAllMediaSizesGeneral(self):
        winTools_operate.selectBasFile(self.basPath)

        #click execute button
        KeyBoardAction.pressF5()
        time.sleep(5)

        printer_operate.exceptionHandlerBeforePrinting()

        winwrapBasicWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
        testsCombos = winGuiAuto.findControls(winwrapBasicWindow, wantedClass="ComboBox")
        print "Find the font selection combo"
        testItems = winGuiAuto.getComboboxItems(testsCombos[0])
        platformItems = winGuiAuto.getComboboxItems(testsCombos[1])
        print len(testItems)
        for i in range(0, len(testItems)):
            if i != 0 :
                #for debug
                #i = 3
                #click execute button
                KeyBoardAction.pressF5()
                time.sleep(8)
                winwrapBasicWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                testsCombos = winGuiAuto.findControls(winwrapBasicWindow, wantedClass="ComboBox")
                testItems = winGuiAuto.getComboboxItems(testsCombos[0])
                platformItems = winGuiAuto.getComboboxItems(testsCombos[1])
            winGuiAuto.selectComboboxItem(testsCombos[0], testItems[i])
            time.sleep(.5)
            for j in range(0, len(platformItems)):
                if j != 0:
                    #click execute button
                    KeyBoardAction.pressF5()
                    time.sleep(8)
                    winwrapBasicWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                    testsCombos = winGuiAuto.findControls(winwrapBasicWindow, wantedClass="ComboBox")
                    testItems = winGuiAuto.getComboboxItems(testsCombos[0])
                    platformItems = winGuiAuto.getComboboxItems(testsCombos[1])
                    winGuiAuto.selectComboboxItem(testsCombos[0], testItems[i])
                winGuiAuto.selectComboboxItem(testsCombos[1], platformItems[j])
                time.sleep(.5)
                okButton = winGuiAuto.findControl(winwrapBasicWindow, wantedClass="Button", wantedText="OK")
                winGuiAuto.clickButton(okButton)
                time.sleep(2)
                if testItems[i] == 'VIP':
                    winwrapBasicWindow1 = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                    paperTypeCombos = winGuiAuto.findControls(winwrapBasicWindow1, wantedClass="ComboBox")
                    paperTypeItems = winGuiAuto.getComboboxItems(paperTypeCombos[0])
                    winGuiAuto.selectComboboxItem(paperTypeCombos[0], random.choice(paperTypeItems))
                    time.sleep(2)
                    editAreas = winGuiAuto.findControls(winwrapBasicWindow1, wantedClass='Edit')
                    printerName = editAreas[2]
                    winGuiAuto.setEditText(printerName, 'abc')
                    time.sleep(2)
                    penSet = editAreas[3]
                    winGuiAuto.setEditText(penSet, '123')
                    time.sleep(2)
                    okButton = winGuiAuto.findControl(winwrapBasicWindow1, wantedClass="Button", wantedText="OK")
                    winGuiAuto.clickButton(okButton)
                    time.sleep(2)
                if testItems[i] == 'MarginsPCL':
                    if j == len(platformItems)-1:
                        break
                    winwrapBasicWindow1 = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                    testsType = winGuiAuto.findControl(winwrapBasicWindow1, wantedClass="ListBox")
                    testListBox = winGuiAuto.getListboxItems(testsType)
                    print "getListboxItems(tests to run)=", testListBox



                    print "Select a test at random"
                    winGuiAuto.selectListboxItem(testsType, random.randint(0, len(testListBox)-1))
                    time.sleep(.5)
                    winGuiAuto.clickButton(winGuiAuto.findControl(winwrapBasicWindow1,wantedClass="Button", wantedText="-->" ))
                    time.sleep(.5)

                    winwrapBasicWindow1 = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                    testsType = winGuiAuto.findControl(winwrapBasicWindow1, wantedClass="ListBox")
                    testListBox = winGuiAuto.getListboxItems(testsType)
                    print "getListboxItems(tests to run)=", testListBox
                    winGuiAuto.selectListboxItem(testsType, random.randint(0, len(testListBox)-1))
                    time.sleep(.5)
                    winGuiAuto.clickButton(winGuiAuto.findControl(winwrapBasicWindow1,wantedClass="Button", wantedText="-->" ))
                    time.sleep(.5)

                    winwrapBasicWindow1 = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                    winGuiAuto.clickButton(winGuiAuto.findControl(winwrapBasicWindow1, wantedClass="Button", wantedText="OK"))
                    time.sleep(2)

                    winTools_operate.clickOKToprint()
                    self.assertTrue(printer_operate.printing_status(), 'Timeout for printing...')

                    winTools_operate.clickOKToprint()
                    self.assertTrue(printer_operate.printing_status(), 'Timeout for printing...')

