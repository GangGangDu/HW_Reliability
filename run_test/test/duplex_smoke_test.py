__author__ = 'maying03'
import  unittest
from run_test.log import Logger
from run_test import winGuiAuto,KeyBoardAction, builder, winTools_operate, printer_operate
import time, random

class duplex_smoke_test(unittest.TestCase):

    def setUp(self):
        self.logger = Logger.getLogger()
        self.builder = builder.TestBuilder.getBuilder()
        self.basPath = self.builder.getScript(case_name = 'duplex_smoke_test')
        winTools_operate.startFlexScript()
    def tearDown(self):
        pass

    def testDuplexSmoke(self):
        winTools_operate.selectBasFile(self.basPath)

        #click execute button
        KeyBoardAction.pressF5()
        time.sleep(5)

        printer_operate.exceptionHandlerBeforePrinting()

        #select yes to save logs append the exist file
        logWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
        yesButton = winGuiAuto.findControl(logWindow, wantedClass="Button", wantedText="Yes")
        winGuiAuto.clickButton(yesButton)
        time.sleep(2)

        #input log name and click ok to continue
        logNameWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
        editAreas = winGuiAuto.findControls(logNameWindow, wantedClass='Edit')
        printerName = editAreas[0]
        winGuiAuto.setEditText(printerName, 'abc')
        time.sleep(2)
        okButton = winGuiAuto.findControl(logNameWindow, wantedClass="Button", wantedText="OK")
        winGuiAuto.clickButton(okButton)
        time.sleep(2)

        selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
        testsType = winGuiAuto.findControl(selectTestsWindow, wantedClass="ListBox")
        testListBox = winGuiAuto.getListboxItems(testsType)
        for i in range(0, len(testListBox)+1):
            if i !=0:
                time.sleep(5)
            if i == len(testListBox):
                selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow,wantedClass="Button", wantedText="Cancel" ))
                break
            testInfo = testListBox[i]
            self.logger.debug("Duplex_smoke_test:Select test to run:%s"%testListBox[i])
            self.logger.debug("Clear selected test...")
            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow,wantedClass="Button", wantedText="Clear" ))
            time.sleep(.5)

            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            testsType = winGuiAuto.findControl(selectTestsWindow, wantedClass="ListBox")
            #testListBox = winGuiAuto.getListboxItems(testsType)

            winGuiAuto.selectListboxItem(testsType, i)
            time.sleep(.5)
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow,wantedClass="Button", wantedText="-->" ))
            time.sleep(.5)

            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow, wantedClass="Button", wantedText="OK"))
            time.sleep(2)
            self.logger.debug("11111111111111:"+testInfo)
            if testInfo == 'USLetter12month.pcl' or testInfo == 'USLetter12month2.pcl':
                self.logger.debug('aaaaaaaaaaaaaaaaa')
                self.assertTrue(printer_operate.printing_status(timeout=710), 'Timeout for printing...')
            if testInfo == 'USLetterBlank.pcl' or testInfo == 'USLetterCompanyName.pcl' or testInfo == 'Brochure.pcl' or testInfo == 'Gettysburg_address.pcl' :
                self.logger.debug('bbbbbbbbbbbbbb')
                self.assertTrue(printer_operate.printing_status(), 'Timeout for printing...')
            if testInfo == 'USLetterCalendar.pcl' or testInfo == 'USLetterCube1.pcl' or testInfo == 'USLetterCube2.pcl':
                self.logger.debug('ccccccccccccccccccccccc')
                self.assertTrue(printer_operate.printing_status(timeout=240), 'Timeout for printing...')
            if testInfo == 'U.S.Constitution.pcl':
                self.logger.debug('ddddddddddddddddddddddd')
                self.assertTrue(printer_operate.printing_status(timeout=5160), 'Timeout for printing...')



