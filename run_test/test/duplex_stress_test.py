__author__ = 'maying03'
import  unittest
from run_test.log import Logger
from run_test import winGuiAuto,KeyBoardAction, builder, winTools_operate, printer_operate
import time, random

class duplex_stress_test(unittest.TestCase):

    def setUp(self):
        self.logger = Logger.getLogger()
        self.builder = builder.TestBuilder.getBuilder()
        self.basPath = self.builder.getScript(case_name = 'duplex_stress_test')
        winTools_operate.startFlexScript()
    def tearDown(self):
        pass

    def testDuplexStress(self):
        winTools_operate.selectBasFile(self.basPath)

        #click execute button
        KeyBoardAction.pressF5()
        time.sleep(5)

        printer_operate.exceptionHandlerBeforePrinting()

        selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
        testsType = winGuiAuto.findControl(selectTestsWindow, wantedClass="ListBox")
        testListBox = winGuiAuto.getListboxItems(testsType)
        for i in range(0, len(testListBox)+1):
            self.logger.debug("cycle number:%d" % i)
            if i != 0:
                time.sleep(8)
            if i == len(testListBox):
                selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
                winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow,wantedClass="Button", wantedText="Cancel" ))
                break
            self.logger.debug("Duplex_stress_test:Select test to run:%s"%testListBox[i])
            self.logger.debug("Clear selected test...")

            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow,wantedClass="Button", wantedText="Clear" ))
            time.sleep(.5)

            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            testsType = winGuiAuto.findControl(selectTestsWindow, wantedClass="ListBox")
            #testListBox = winGuiAuto.getListboxItems(testsType)

            winGuiAuto.selectListboxItem(testsType, i)
            time.sleep(.5)
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow,wantedClass="Button", wantedText="-->" ))
            time.sleep(.5)

            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow, wantedClass="Button", wantedText="OK"))
            time.sleep(2)

            #Ensure paper size, the press OK to print
            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow, wantedClass="Button", wantedText="OK"))
            time.sleep(2)

            selectTestsWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
            winGuiAuto.clickButton(winGuiAuto.findControl(selectTestsWindow, wantedClass="Button", wantedText="OK"))
            time.sleep(2)

            #self.assertTrue(printer_operate.printing_status_type2(), 'Timeout for printing...')

