__author__ = 'maying03'
import builder, winGuiAuto, printer_operate
import time, types, os, getpass, shutil
import KeyBoardAction, semiauto
from log import Logger

builder1 = builder.TestBuilder.getBuilder()
logger = Logger.getLogger()
def captureCoredump(log_path):
    topWindow = winGuiAuto.findTopWindow(wantedClass='#32770')
    if type(topWindow) is types.ListType:
        winGuiAuto.startApplication(builder1.getConfig(property = 'coredump_path'))
        time.sleep(5)
        topWindow = winGuiAuto.findTopWindow(wantedClass='#32770')
    winGuiAuto.setAppForeground(topWindow)
    time.sleep(3)
    winGuiAuto.clickButton(winGuiAuto.findControl(topWindow, wantedText='Find devices', wantedClass='Button'))
    time.sleep(2)
    winGuiAuto.clickButton(winGuiAuto.findControl(topWindow, wantedText='Capture coredump', wantedClass='Button'))
    time.sleep(30)
    coredump_folder = 'C:/HPCoreDump/'
    l=os.listdir(coredump_folder)
    l.sort(key=lambda fn: os.path.getmtime(coredump_folder+fn) if not os.path.isdir(coredump_folder+fn) else 0)
    shutil.copy('%s%s'%(coredump_folder, l[-1]),log_path)
    time.sleep(2)

def makedir(path):
    if os.path.exists(path):
        shutil.rmtree(path)
    os.mkdir(path)

def captureSiftLogs(log_path):
    topWindow = winGuiAuto.findTopWindow(wantedClass='Qt5QWindowIcon')
    winGuiAuto.setAppForeground(topWindow)
    time.sleep(3)
    winGuiAuto.clickPosition(1088, 68)
    time.sleep(2)
    sift_log_folder = '%s%s%s'%('C:/Users/', getpass.getuser(), '/')
    l=os.listdir(sift_log_folder)
    l.sort(key=lambda fn: os.path.getmtime(sift_log_folder+fn) if not os.path.isdir(sift_log_folder+fn) else 0)
    shutil.copy('%s%s'%(sift_log_folder, l[-1]),log_path)
    time.sleep(2)


def startCoredump():
    topWindow = winGuiAuto.findTopWindow(wantedClass='#32770',wantedText='hp coredump capture - 3.3')
    if type(topWindow) is types.ListType:
        winGuiAuto.startApplication(builder1.getConfig(property = 'coredump_path'))
        time.sleep(5)
        topWindow = winGuiAuto.findTopWindow(wantedClass='#32770',wantedText='hp coredump capture - 3.3')
    winGuiAuto.setAppForeground(topWindow)
    time.sleep(3)

def startFlexScript():
    topWindow = winGuiAuto.findTopWindow(wantedClass='WindowsForms10.Window.8.app.0.141b42a_r10_ad1')
    time.sleep(2)
    if type(topWindow) is types.ListType:
        winGuiAuto.startApplication(builder1.getConfig(property = 'flexscript_path'))
        time.sleep(15)
        topWindow = winGuiAuto.findTopWindow(wantedClass='WindowsForms10.Window.8.app.0.141b42a_r10_ad1')
    winGuiAuto.setAppForeground(topWindow)
    time.sleep(5)

def startSift():
    topWindow = winGuiAuto.findTopWindow(wantedClass='Qt5QWindowIcon')
    if type(topWindow) is types.ListType:
        winGuiAuto.startApplication(builder1.getConfig(property = 'sift_path'))
        time.sleep(5)
        topWindow = winGuiAuto.findTopWindow(wantedClass='Qt5QWindowIcon')
    winGuiAuto.setAppForeground(topWindow)
    time.sleep(3)
    winGuiAuto.clickPosition(811, 68)
    time.sleep(2)




def clickOKToprint():
    winwrapBasicWindow = winGuiAuto.findTopWindow(wantedText='WinWrap Basic')
    winGuiAuto.clickButton(winGuiAuto.findControl(winwrapBasicWindow, wantedClass="Button", wantedText="OK"))
    time.sleep(2)


def selectBasFile(basPath):
    winGuiAuto.findTopWindow(wantedClass='WindowsForms10.Window.8.app.0.141b42a_r10_ad1')
    KeyBoardAction.ctrlPlus('O')
    time.sleep(1)
    openDialog = winGuiAuto.findTopWindow(wantedText='Open')

    #select script
    winGuiAuto.dumpWindow(openDialog)
    editArea = winGuiAuto.findControl(openDialog, wantedClass='Edit')
    winGuiAuto.setEditText(editArea, basPath)
    time.sleep(2)
    okButton = winGuiAuto.findControl(openDialog, wantedClass="Button", wantedText="Open")
    winGuiAuto.clickButton(okButton)
    time.sleep(1)


def checkDialogDisplayed(wantedText = None, wantedControlClass=None, wantedControlText=None):
    try:
        test_window = winGuiAuto.findTopWindow(wantedText=wantedText)
        winGuiAuto.findControl(test_window, wantedClass=wantedControlClass, wantedText = wantedControlText)
        logger.debug('Has found window: %s and control:%s %s'%(wantedText, wantedControlClass, wantedControlText))
        return True
    except:
        logger.debug('Can not find window: %s or control:%s %s'%(wantedText, wantedControlClass, wantedControlText))
        return False