'''
Created on 2013-12-04

@author: yma
'''
#coding:GBK
import time
from unittest import TestResult
from log import Logger
from monitor.monitorCoreDump import coredumpMonitor
import winTools_operate
#from builder import TestBuilder

class TestRunner(object):
    '''
    Class for loading test runner
    '''
    @staticmethod
    def getRunner():
        return PYTestRunner()

class PYTestRunner(object):
    def __init__(self):
        '''Init the instance of TestRunner'''
        self.logger = Logger.getLogger()


    def _makeResult(self):
        return _TestResult()

    def run(self, test):
        #check coredump every 60
        coredumpMonitor(60).start()
        #result output terminal
        result = self._makeResult()
        #test start time
        startTime = time.time()
        #if test is instance of TestSuite:for t in test: i(result)
        #run test/testsuite
        test(result)
        #test stop time
        stopTime = time.time()
        #test duration
        timeTaken = stopTime - startTime
        #if not self.verbosity:
        #print all erros during test
        result.printErrors()
          #----------------
        #self.logger.debug(result.separator2)
        #total case number has been ran
        #run = result.testsRun
        #self.logger.debug("Total ran %d test%s in %.3fs" % (run, run != 1 and "s" or "", timeTaken))
        #space line output
        #If test include failures or errors . special notification for failure and error
        if not result.wasSuccessful():
            self.logger.debug("FAILED (")
            failed, errored = map(len, (result.failures, result.errors))
            if failed:
                self.logger.debug("failures=%d" % failed)
            if errored:
                if failed:
                    self.logger.debug(", ")
                self.logger.debug("errors=%d" % errored)
            self.logger.debug(")")
        #else:
        #    self.logger.debug("OK")
        return result

def collectResult(func):         
    def wrap(*args, **argkw):
        func(*args, **argkw)
        return func
    return wrap

class _TestResult(TestResult):
    separator1 = '=' * 70 
    separator2 = '-' * 70 
    
    def __init__(self, stream=None, descriptions=None, verbosity=None):
        TestResult.__init__(self)
        self.logger = Logger.getLogger()
        #self.builder = TestBuilder.getBuilder()
    
    @collectResult
    def startTest(self, test):
        TestResult.startTest(self, test)
        #creat log folder for test
        test_name  = type(test).__name__
        self.test_result_folder = '%s%s%s%s%s'%("report", "/", 'logs','/', test_name)
        winTools_operate.makedir(self.test_result_folder)
        winTools_operate.startSift()
        winTools_operate.startCoredump()
        #need to add write and writln method for terminal output
        self.logger.debug(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
        self.logger.debug(self.getDescription(test)) 
        self.logger.debug("START")
    
    @collectResult   
    def addSuccess(self,test):
        TestResult.addSuccess(self, test) 
        self.logger.debug("PASS")
        #self.builder.setSuccess()
        winTools_operate.captureSiftLogs(self.test_result_folder)

    @collectResult
    def addFailure(self,test,err):
        TestResult.addFailure(self, test, err)
        self.logger.debug("FAIL") 
        #self.builder.setFailure()
        winTools_operate.captureSiftLogs(self.test_result_folder)
     
    @collectResult    
    def addError(self, test, err):
        TestResult.addError(self, test, err)
        self.logger.debug("ERROR")
        #self.builder.setError()
        winTools_operate.captureSiftLogs(self.test_result_folder)

    #@writeResult
    def stopTest(self, test):
        TestResult.stopTest(self, test)
        self.logger.debug('STOP')

    def getDescription(self, test):
        return test.shortDescription() or str(test)
            
    def printErrors(self):
        self.printErrorList('ERROR', self.errors) 
        self.printErrorList('FAIL', self.failures)
  
    def printErrorList(self, flavour, errors):
        for test, err in errors:
            self.logger.debug(self.separator1)
            self.logger.debug("%s: %s" % (flavour,self.getDescription(test)))
            self.logger.debug(self.separator2)
            self.logger.debug("%s" % err) 
