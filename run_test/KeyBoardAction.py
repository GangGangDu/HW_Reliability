# -*- coding: utf-8 -*-
__author__ = 'Administrator'

import win32con,win32api
import time


def presskeybykeyvalue(v):
    #PubCommonLib.updatelog("press key by keyvalue %s" % str(v))
    win32api.keybd_event(v,0,0,0)
    time.sleep(1)
    win32api.keybd_event(v,0,win32con.KEYEVENTF_KEYUP,0)
    time.sleep(1)

def presskeybychar(ch):
    #PubCommonLib.updatelog("press %s" % ch)
    win32api.keybd_event(ord(ch),0,0,0)
    win32api.keybd_event(ord(ch),0,win32con.KEYEVENTF_KEYUP,0)

def pressF5():
    win32api.keybd_event(116,0,0,0)  #F5 is 116
    win32api.keybd_event(116,0,win32con.KEYEVENTF_KEYUP,0)

def pressaltF4():
    '''
    get the acess key (alt + F4)
    '''
    #PubCommonLib.updatelog("press alt + %s" % "F4")
    win32api.keybd_event(18,0,0,0)  #ctrl是17
    time.sleep(0.5)
    win32api.keybd_event(115,0,0,0)
    time.sleep(0.5)
    win32api.keybd_event(115,0,win32con.KEYEVENTF_KEYUP,0) #释放
    time.sleep(0.5)
    win32api.keybd_event(18,0,win32con.KEYEVENTF_KEYUP,0)
    time.sleep(0.5)

def pressEnter():
    #PubCommonLib.updatelog("press Enter")
    win32api.keybd_event(13,0,0,0)
    win32api.keybd_event(13,0,win32con.KEYEVENTF_KEYUP,0)

def pressESC():
    #PubCommonLib.updatelog("press ESC")
    win32api.keybd_event(27,0,0,0)
    win32api.keybd_event(27,0,win32con.KEYEVENTF_KEYUP,0)


def ctrlPlus(char):
    '''
    get the acess key (ctrl + char)
    '''
    #PubCommonLib.updatelog("press ctrl + %s" % char)
    win32api.keybd_event(17,0,0,0)  #ctrl是17
    win32api.keybd_event(ord(char),0,0,0)
    win32api.keybd_event(ord(char),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(17,0,win32con.KEYEVENTF_KEYUP,0)

def ctrlTab():
    '''
    get the acess key (ctrl + Table)
    '''
    #PubCommonLib.updatelog("press ctrl + %s" % "Tab")
    win32api.keybd_event(17,0,0,0)  #ctrl是17
    win32api.keybd_event(9,0,0,0) #Tab是9
    win32api.keybd_event(9,0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(17,0,win32con.KEYEVENTF_KEYUP,0)

def Tab(n):
    '''
    get the acess key (ctrl + Table)
    '''
    for i in range(n):
        win32api.keybd_event(9,0,0,0) #Tab是9
        win32api.keybd_event(9,0,win32con.KEYEVENTF_KEYUP,0) #释放

def tabEnter(n):
    '''
    get the acess key (n(Table) + Enter)
    '''
    #PubCommonLib.updatelog("press Tab for %s times and press Enter" % str(n))
    for i in range(n):
        win32api.keybd_event(9,0,0,0) #Tab是9
        win32api.keybd_event(9,0,win32con.KEYEVENTF_KEYUP,0) #释放
    time.sleep(1)
    win32api.keybd_event(13,0,0,0)
    win32api.keybd_event(13,0,win32con.KEYEVENTF_KEYUP,0) #释放

def altPlus(char):
    '''
    get the acess key (alt + char)
    '''
    #PubCommonLib.updatelog("press alt + %s" % char)
    win32api.keybd_event(18,0,0,0)  #alt是17
    win32api.keybd_event(ord(char),0,0,0)
    win32api.keybd_event(ord(char),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(18,0,win32con.KEYEVENTF_KEYUP,0)

def altFPN():
    '''
    get the acess key (alt + char)
    '''
    #PubCommonLib.updatelog("press alt + F + N + P")
    win32api.keybd_event(18,0,0,0)  #alt是17
    win32api.keybd_event(ord("F"),0,0,0)
    win32api.keybd_event(ord("P"),0,0,0)
    win32api.keybd_event(ord("N"),0,0,0)
    win32api.keybd_event(ord("F"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("P"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("N"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(18,0,win32con.KEYEVENTF_KEYUP,0)

def selectPrinter():
    '''
    get the acess key (alt + char)
    '''
    #PubCommonLib.updatelog("press alt + F + P + I to select printer")
    win32api.keybd_event(18,0,0,0)  #alt是17
    win32api.keybd_event(ord("F"),0,0,0)
    win32api.keybd_event(ord("P"),0,0,0)
    win32api.keybd_event(ord("I"),0,0,0)
    win32api.keybd_event(ord("F"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("P"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("I"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(18,0,win32con.KEYEVENTF_KEYUP,0)
    for i in range(2):
        win32api.keybd_event(40,0,0,0) #down is 40
        win32api.keybd_event(40,0,win32con.KEYEVENTF_KEYUP,0) #release
    win32api.keybd_event(13,0,0,0)
    win32api.keybd_event(13,0,win32con.KEYEVENTF_KEYUP,0) #释放

def upKey():
    win32api.keybd_event(38,0,0,0) #up is 38
    win32api.keybd_event(38,0,win32con.KEYEVENTF_KEYUP,0) #release

def ndownKey(n):
    for i in range(n):
        win32api.keybd_event(40,0,0,0) #down is 40
        win32api.keybd_event(40,0,win32con.KEYEVENTF_KEYUP,0) #release

def tab():
    win32api.keybd_event(9,0,0,0) #Tab是9
    win32api.keybd_event(9,0,win32con.KEYEVENTF_KEYUP,0) #释放

def ntab(n):
    for i in range(n):
        win32api.keybd_event(9,0,0,0) #Tab是9
        win32api.keybd_event(9,0,win32con.KEYEVENTF_KEYUP,0) #释放

def clearprintNumer():
    win32api.keybd_event(39,0,0,0) #Tab是9
    win32api.keybd_event(39,0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(39,0,0,0) #Tab是9
    win32api.keybd_event(39,0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(39,0,0,0) #Tab是9
    win32api.keybd_event(39,0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(8,0,0,0) #Tab是9
    win32api.keybd_event(8,0,win32con.KEYEVENTF_KEYUP,0) #释放

######################################################################################################
def importKey():
    PubCommonLib.updatelog("press alt + F + O + I to import outlook files")
    win32api.keybd_event(18,0,0,0)  #alt是17
    win32api.keybd_event(ord("F"),0,0,0)
    win32api.keybd_event(ord("O"),0,0,0)
    win32api.keybd_event(ord("I"),0,0,0)
    win32api.keybd_event(ord("F"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("O"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("I"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(18,0,win32con.KEYEVENTF_KEYUP,0)


def ctrlshiftI():
    #PubCommonLib.updatelog("press ctrl + shift  + I")
    win32api.keybd_event(17,0,0,0) #Tab是9
    win32api.keybd_event(16,0,0,0) #Tab是9
    win32api.keybd_event(ord("I"),0,0,0) #Tab是9
    win32api.keybd_event(17,0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(16,0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("I"),0,win32con.KEYEVENTF_KEYUP,0) #释放

def ctrlAP():
    '''
    get the acess key (alt + char)
    '''
    #PubCommonLib.updatelog("press ctrl + A  + P")
    win32api.keybd_event(17,0,0,0)  #alt是17
    win32api.keybd_event(ord("A"),0,0,0)
    win32api.keybd_event(ord("P"),0,0,0)
    win32api.keybd_event(ord("A"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("P"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(17,0,win32con.KEYEVENTF_KEYUP,0)

def altPR():
    '''
    get the acess key (alt + char)
    '''
    #PubCommonLib.updatelog("press alt +F+ R  + P")
    win32api.keybd_event(17,0,0,0)  #alt是17
    win32api.keybd_event(ord("P"),0,0,0)
    win32api.keybd_event(ord("R"),0,0,0)
    win32api.keybd_event(ord("R"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("P"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(17,0,win32con.KEYEVENTF_KEYUP,0)

def pressDelete():
    win32api.keybd_event(46,0,0,0)  #delete是46
    win32api.keybd_event(46,0,win32con.KEYEVENTF_KEYUP,0)

def altFPRoutlook():
    '''
    get the acess key (alt + char)
    '''
    #PubCommonLib.updatelog("press alt F+P+R")
    win32api.keybd_event(18,0,0,0)  #alt是18
    win32api.keybd_event(ord("F"),0,0,0)
    win32api.keybd_event(ord("P"),0,0,0)
    win32api.keybd_event(ord("R"),0,0,0)
    win32api.keybd_event(ord("F"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("P"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(ord("R"),0,win32con.KEYEVENTF_KEYUP,0) #释放
    win32api.keybd_event(18,0,win32con.KEYEVENTF_KEYUP,0)