__author__ = 'maying03'
from run_test.builder import TestBuilder
from run_test import Configure
import os, time
from run_test.HTMLTestRunner import HTMLTestRunner
from run_test.log import Logger

def run_tests(all_suite):
    Configure.REPORT_DIR =os.path.abspath(os.path.dirname(__file__) + os.sep + "report" + os.sep +time.strftime("executeResult_%Y-%m-%d-%H-%M-%S",time.localtime()))
    os.makedirs( Configure.REPORT_DIR)
    reportName = os.path.join(Configure.REPORT_DIR,"./result.html")
    Logger("DEBUG")
    fp = file(reportName, 'wb')
    runner = HTMLTestRunner(stream=fp,title='HP Mobile Testing Report',description='Script Running:')
    runner.run(all_suite)

if __name__ == '__main__':

    builder = TestBuilder.getBuilder()
    run_tests(builder.loadTestSuites())

